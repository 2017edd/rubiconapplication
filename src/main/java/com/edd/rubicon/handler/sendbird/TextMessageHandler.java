package com.edd.rubicon.handler.sendbird;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.edd.rubicon.models.sendbird.database.OpenChannelMessages;
import com.edd.rubicon.models.sendbird.database.OpenChannelMessagesRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Handles making all text message requests to the SendBird API. A text message is defined as a message
 * that contains only text (does not contain files) and is not sent by a SendBird admin
 * @author Collin Bolles
 * 3/16/2018
 */
public class TextMessageHandler {

	private String api_url;
	
	private MultiValueMap<String, String> headers;
	
	/**
	 * Must be called to begin and text message handling. The api_url and api_token are required to make
	 * requets to SendBird and should be stored in a property file
	 */
	public TextMessageHandler(String api_url, String api_token) {
		this.api_url = api_url;
		
		headers = new LinkedMultiValueMap<>();
		headers.add("api-token", api_token);
	}
	
	/**
	 * Used to update the database with the new message that was sent. The method checks to see if
	 * <ul>
	 * <li>The channel has existed before</li>
	 * <li>The channel has a List of messages already</li>
	 * </ul>
	 * The method then either creates a new channel object then adds the new message, or just adds 
	 * the new message to an existing channel object.
	 * @precondition The openChannelRepository object must be fully functional and body must be a
	 * correctly formatted SendBird response object containing the newly created message
	 * @param body {String} SendBird JSON object containing the newly created message
	 * @param openChannelRepository {OpenChannelMessagesRepository} spring boot generated interface with
	 * the mongoDB
	 */
	private void updateChannelMessages(String body, OpenChannelMessagesRepository openChannelRepository) {
		try {
			HashMap<String, Object> parsedBody = new ObjectMapper().readValue(body, HashMap.class);
			
			OpenChannelMessages openChannelMessages = openChannelRepository.findByChannelUrl( (String) parsedBody.get("channel_url"));
			if(openChannelMessages == null) {
				List<Long> messages = new ArrayList<Long>();
				messages.add(((Integer)parsedBody.get("message_id")).longValue());
				openChannelMessages = new OpenChannelMessages( (String) parsedBody.get("channel_url"), messages);
			}
			else if(openChannelMessages.getMessageIds() == null) {
				List<Long> messages = new ArrayList<Long>();
				messages.add(((Integer)parsedBody.get("message_id")).longValue());
				openChannelMessages.setMessageIds(messages);
			}
			else 
				openChannelMessages.getMessageIds().add(((Integer)parsedBody.get("message_id")).longValue());
			openChannelRepository.save(openChannelMessages);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Used to send a text message from a user. Text messages are sent to any chat type (direct, open channel,
	 * or group) and contain only text (no files). The string passed in should be a correct json object to make 
	 * a SendBird text request
	 * @param messageRequest {String} A JSON object representing the text request
	 * <br>
	 * Should be in the following format
	 * <br>
	 * { <br>
	 *     "message_type": "MESG", <br>
	 *     "user_id":"SendBird User ID", <br>
	 *     "message":"Message Content" <br>
	 *     "channel_url": "SendBird Channel URL" <br>
	 * }
	 * @param openChannelRepository 
	 * @precondition messageRequest is not null and contains valid json in the form above
	 * @return {String} String representation of the SendBird response which is the SendBird text that was sent.
	 * Returns null if there is a failure sending the message
	 */
	public String sendTextMessage(String messageRequest, OpenChannelMessagesRepository openChannelRepository) {
		/*
		 * TODO
		 * Add encryption to messages being sent
		 */
		RestTemplate restTemplate = new RestTemplate();
		
		HttpEntity<String> entity = new HttpEntity<>(messageRequest, headers);
		HashMap<String, String> result;
		try {
			result = new ObjectMapper().readValue(messageRequest, HashMap.class);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		/*
		 * TODO
		 * Change url to dynamically pick between group messages and open channel messages
		 */
		String requestUrl = api_url + "open_channels/" + result.get("channel_url") + "/messages";
		/*
		 * TODO
		 * Catch errors where SendBird returns an error (which wouldn't be a OpenChannel object)
		 */
		 String sendBirdResponse =  restTemplate.exchange(requestUrl, HttpMethod.POST, entity, String.class).getBody();
		 updateChannelMessages(sendBirdResponse, openChannelRepository);
		 return sendBirdResponse;
	}
	
	/**
	 * Used to get all of the text messages from the SendBird api. The messages are collected
	 * from looping through the database to collect messages IDS then making the SendBird calls
	 * to get the individual messages from the IDS
	 * @param channelUrl {String} Sendbird channel url for the given message channel
	 * @param openChannelRepository {OpenChannelMessagesRepository} Interface with the mongoDB
	 * @return
	 */
	public String getTextMessages(String channelUrl, OpenChannelMessagesRepository openChannelRepository) {
		RestTemplate restTemplate = new RestTemplate();
		
		List<Long> messageIds = openChannelRepository.findByChannelUrl(channelUrl).getMessageIds();
		String response = "{"
					+ "\"messages\" : [";
		HttpEntity<String> entity = new HttpEntity<>(headers);
		for(Long id : messageIds) {
			String requestUrl = api_url + "open_channels/" + channelUrl + "/messages/" + id;
			response += restTemplate.exchange(requestUrl, HttpMethod.GET, entity, String.class).getBody() + ",";
		}
		response = response.substring(0, response.length()-1) + " ] }";
		return response;
	}
}
