package com.edd.rubicon.handler.sendbird;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.edd.rubicon.models.sendbird.TextMessage;
import com.edd.rubicon.models.sendbird.database.OpenChannelMessagesRepository;

public class TextMessageWorker extends Thread {
	
	private String api_url;
	private String api_token;
	private OpenChannelMessagesRepository openChannelRepository;
	private TextMessage textMessage;
	
	public TextMessageWorker(String api_url, String api_token, OpenChannelMessagesRepository openChannelRepository, TextMessage textMessage) {
		this.api_url = api_url;
		this.api_token = api_token;
		this.openChannelRepository = openChannelRepository;
		this.textMessage = textMessage;
	}
	
	public void run() {
		TextMessageHandler textMessageHandler = new TextMessageHandler(api_url, api_token);
		String messageData = textMessage.getBasicDataJSON();
		new ResponseEntity<Object>(textMessageHandler.sendTextMessage(messageData, openChannelRepository), HttpStatus.OK);
	}
}
