package com.edd.rubicon.service;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.apache.tomcat.util.codec.binary.Base64;

/**
 * Message encryption/decryption class
 * @author Erick Simonetti
 * 03/14/18
 */

public class MessageEncryption {
	private String strKey;
	/**
	 * Constructs the class with the given encryption key, key should be loaded from property file
	 * @param strKey {String} encryption key
	 */
	public MessageEncryption(String strKey) {
		this.strKey = strKey;
	}
	
	/**
	 * Encrypts the given text using the key passed into the constructor, uses Blowfish
	 * encryption
	 * @precondition strClearText is not null 
	 * @postcondition returns encrypted String if successful, null if encryption failed
	 * @param strClearText {String} string that will be encrypted
	 * @return {String} encrypted string
	 */
	public String encryptMessage(String strClearText) {
		String str = "";
		try {
			SecretKeySpec skeyspec=new SecretKeySpec(strKey.getBytes(),"Blowfish");
			Cipher cipher=Cipher.getInstance("Blowfish");
			cipher.init(Cipher.ENCRYPT_MODE, skeyspec);
			byte[] encrypted=cipher.doFinal(strClearText.getBytes());
			str = Base64.encodeBase64String(encrypted);
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return str;
	}
	
	/**
	 * Decrypt string passed into the method using the strKey passed into the constructor 
	 * @precondition encrypted is not null 
	 * @postcondition returns decrypted String if successfully decrypted, null if failed
	 * @param encrypted {String} string to be decrypted
	 * @return {String} decrypted string
	 */
	public String decryptMessage(String encrypted){
		String strData="";
		try {
			SecretKeySpec skeyspec=new SecretKeySpec(strKey.getBytes(),"Blowfish");
			Cipher cipher=Cipher.getInstance("Blowfish");
			cipher.init(Cipher.DECRYPT_MODE, skeyspec);
			byte[] encryptedBytes = Base64.decodeBase64(encrypted);
			byte[] decrypted=cipher.doFinal(encryptedBytes);
			strData=new String(decrypted);
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return strData;
	}
}
