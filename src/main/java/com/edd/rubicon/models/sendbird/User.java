package com.edd.rubicon.models.sendbird;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Java object that represents the User object used by SendBird. The object is constructed automatically
 * by jackson from SendBird REST requests
 * @author Collin Bolles
 * 2/1/2018
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class User implements SendBird {
	private String user_id;
	private String nickname;
	private String profile_url;
	
	public User() {
		
	}
	
	public User(String user_id) {
		this.user_id = user_id;
	}

	public String getUser_id() {
		return user_id;
	}



	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}



	public String getNickname() {
		return nickname;
	}



	public void setNickname(String nickname) {
		this.nickname = nickname;
	}



	public String getProfile_url() {
		return profile_url;
	}



	public void setProfile_url(String profile_url) {
		this.profile_url = profile_url;
	}

	@Override
	public String asJSON() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return(mapper.writeValueAsString(this));
		} catch (JsonProcessingException e) {
			return null;
		}
	}

}
