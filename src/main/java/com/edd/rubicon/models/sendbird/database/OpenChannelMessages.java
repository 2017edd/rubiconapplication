package com.edd.rubicon.models.sendbird.database;

import java.util.List;

import org.springframework.data.annotation.Id;

public class OpenChannelMessages {
	
	@Id
	private String channelUrl;
	private List<Long> messageIds;
	
	public OpenChannelMessages(String channelUrl, List<Long> messageIds) {
		this.channelUrl = channelUrl;
		this.messageIds = messageIds;
	}

	public String getChannelUrl() {
		return channelUrl;
	}

	public void setChannelUrl(String channelUrl) {
		this.channelUrl = channelUrl;
	}

	public List<Long> getMessageIds() {
		return messageIds;
	}

	public void setMessageIds(List<Long> messageIds) {
		this.messageIds = messageIds;
	}
	
	public String toString() {
		String messages = "";
		for(Long messageId : messageIds)
			messages += " " + messageId + " ";
		return channelUrl + messages;
	}
}
