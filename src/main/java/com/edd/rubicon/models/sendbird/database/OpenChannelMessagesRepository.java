package com.edd.rubicon.models.sendbird.database;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface OpenChannelMessagesRepository extends MongoRepository<OpenChannelMessages, String> { 

    public OpenChannelMessages findByChannelUrl(String channel_url);
    
}
