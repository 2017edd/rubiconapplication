package com.edd.rubicon.models.sendbird;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Java object that represents the TextMessage object used by SendBird. The object is constructed automatically
 * by jackson from SendBird REST requests
 * @author Collin Bolles
 * 2/1/2018
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TextMessage implements SendBird {

	private long message_id;
	private String type;
	private String message;
	private String custom_type;
	private String data;
	private User user;
	private String channel_url;
	private long created_at;
	private long updated_at;
	private File file;
	
	public TextMessage() {
		
	}
	
	public long getMessage_id() {
		return message_id;
	}


	public void setMessage_id(long message_id) {
		this.message_id = message_id;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public String getCustom_type() {
		return custom_type;
	}


	public void setCustom_type(String custom_type) {
		this.custom_type = custom_type;
	}


	public String getData() {
		return data;
	}


	public void setData(String data) {
		this.data = data;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public String getChannel_url() {
		return channel_url;
	}


	public void setChannel_url(String channel_url) {
		this.channel_url = channel_url;
	}


	public long getCreated_at() {
		return created_at;
	}


	public void setCreated_at(long created_at) {
		this.created_at = created_at;
	}


	public long getUpdated_at() {
		return updated_at;
	}


	public void setUpdated_at(long updated_at) {
		this.updated_at = updated_at;
	}


	public File getFile() {
		return file;
	}


	public void setFile(File file) {
		this.file = file;
	}
	
	public String getBasicDataJSON() {
		String data = "{" 
						+ "\"message_type\"" + ":" + "\"MESG\","
						+ "\"type\"" + ":" + "\"CHAT\","
						+ "\"user_id\"" + ":" + "\""+this.user.getUser_id()+"\","
						+ "\"message\"" + ":" + "\""+message+"\","
						+ "\"channel_url\"" + ":" + "\""+channel_url+"\""
						+ "}";
		return data;				
	}
	
	public String asJSON() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return(mapper.writeValueAsString(this));
		} catch (JsonProcessingException e) {
			return null;
		}
	}

}
