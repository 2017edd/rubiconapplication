package com.edd.rubicon.controller.UI;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller that handles all UI (HTML, CSS, Javascript) that is send to the client for text messaging
 * @author Collin Bolles
 *
 */
@Controller
public class TextMessageUIController {
	
	/**
	 * Handles returning the HTML of the message interface to the client code.
	 * @param model 
	 * @return HTML of ther message interface
	 */
	@RequestMapping(value= "/directMessage")
    public String directMessagePage(Model model) {
        return "messaging/directMessage";
    }
	
	/**
	 * Handles returning the HTML of the message interface to the client code.
	 * @param model 
	 * @return HTML of ther message interface
	 */
	@RequestMapping(value= "/testMessage")
    public String directMessageTest(Model model) {
        return "messaging/messagingTest";
    }

}
