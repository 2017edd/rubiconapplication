package com.edd.rubicon.controller.sendbird;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.edd.rubicon.handler.sendbird.TextMessageHandler;
import com.edd.rubicon.models.sendbird.database.OpenChannelMessagesRepository;

/**
 * Controller that interfaces with the javascript that runs on the client side to send messages through SendBird.
 * All requests to interact with a message (editing, deleting, sending) will be made through this controller
 * @author Collin Bolles
 *
 */
@RestController
public class TextMessageController {
	
	@Value("${sendbird.api.url}")
	private String api_url;
	
	@Value("${sendbird.api.token}")
	private String api_token;
	
	@Autowired
	private OpenChannelMessagesRepository openChannelRepository;
	
	/**
	 * Controller called when sending a text message through SendBird. Should not be called from within the server
	 * 
	 * @param body {String} JSON representation of the text message request. See documentation of SendBird text messages
	 * @return {String} JSON representation of SendBird response object
	 */
	@CrossOrigin(origins = "http://localhost:8080") //TODO add support for ip address for phonegap
    @RequestMapping(value = "/sendMessage", produces = "application/json")
    public ResponseEntity<Object> sendTextMessage(@RequestBody String body) {
    		TextMessageHandler textMessageHandler = new TextMessageHandler(api_url, api_token);
		try {
			body = java.net.URLDecoder.decode(body, "UTF-8");
			return new ResponseEntity<Object>(textMessageHandler.sendTextMessage(body.substring(0, body.length()-1), openChannelRepository), HttpStatus.OK);
		} catch (UnsupportedEncodingException e) {
			/*
			 * TODO
			 * Add error handling when URI cannot be decoded
			 */
			e.printStackTrace();
			return null;
		}
    }
	
	/**
	 * Controller called when the page needs to get messages sent on a given channel
	 * 
	 * @param channelUrl {String} The SendBird channel url for the messages to be pulled
	 * @return {String} JSON array of messages from the given channel
	 */
	@CrossOrigin(origins = "http://localhost:8080") //TODO add support for ip address for phonegap
    @RequestMapping(value = "/getMessages", produces = "application/json")
    public ResponseEntity<Object> getTextMessages(@RequestParam(name="channel_url", required=true) String channelUrl) {
    		TextMessageHandler textMessageHandler = new TextMessageHandler(api_url, api_token);
		return new ResponseEntity<Object>(textMessageHandler.getTextMessages(channelUrl, openChannelRepository), HttpStatus.OK);
    }
	
}
