package com.edd.rubicon.controller.messaging;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;

import com.edd.rubicon.handler.sendbird.TextMessageHandler;
import com.edd.rubicon.handler.sendbird.TextMessageWorker;
import com.edd.rubicon.models.sendbird.TextMessage;
import com.edd.rubicon.models.sendbird.database.OpenChannelMessagesRepository;

@Controller
public class MessageController {
	
	@Value("${sendbird.api.url}")
	private String api_url;
	
	@Value("${sendbird.api.token}")
	private String api_token;
	
	@Autowired
	private OpenChannelMessagesRepository openChannelRepository;

    @MessageMapping("/chat.sendMessage")
    @SendTo("/topic/public")
    public TextMessage greeting(@Payload TextMessage textMessage) throws Exception {
    		TextMessageWorker textMessageWorker = new TextMessageWorker(api_url, api_token, openChannelRepository, textMessage);
    		textMessageWorker.run();
        return textMessage;
    }
    
    @MessageMapping("/chat.addUser")
    @SendTo("/topic/public")
    public TextMessage addUser(@Payload TextMessage chatMessage,
                               SimpMessageHeaderAccessor headerAccessor) {
        headerAccessor.getSessionAttributes().put("username", chatMessage.getUser().getUser_id());
        return chatMessage;
    }
}