'use strict';

var usernamePage = document.querySelector('#username-page');
var chatPage = document.querySelector('#chat-page');
var usernameForm = document.querySelector('#usernameForm');
var messageForm = document.querySelector('#messageForm');
var messageInput = document.querySelector('#message');
var messageArea = document.querySelector('#messageArea');
var connectingElement = document.querySelector('.connecting');

var stompClient = null;
var username = null;

var colors = [
    '#2196F3', '#32c787', '#00BCD4', '#ff5652',
    '#ffc107', '#ff85af', '#FF9800', '#39bbb0'
];

function connect(event) {
	var url_string = window.location.href;
	var url = new URL(url_string);
	try{
		username = url.searchParams.get("username") == null ? 'collin_bolles' : url.searchParams.get("username");
	}
    catch(e) {
    	console.log(e);
    		username = 'collin_bolles';
    }
    console.log(username);

    if(username) {
        chatPage.classList.remove('hidden');

        var socket = new SockJS('/ws');
        stompClient = Stomp.over(socket);

        stompClient.connect({}, onConnected, onError);
    }
}


function onConnected() {
    // Subscribe to the Public Topic
    stompClient.subscribe('/topic/public', onMessageReceived);

    // Tell your username to the server
    stompClient.send("/app/chat.addUser",
        {},
        JSON.stringify({ 
            "type" : "JOIN",
            "user" : {
            		"user_id" : username
            },
            "channel_url" : "sendbird_open_channel_31836_03f9acb1ca94937eff8ed904ba2d2748cbcd9fd9"
        })
    )

    connectingElement.classList.add('hidden');
}


function onError(error) {
    connectingElement.textContent = 'Could not connect to WebSocket server. Please refresh this page to try again!';
    connectingElement.style.color = 'red';
}


function sendMessage(event) {
    var messageContent = messageInput.value.trim();

    if(messageContent && stompClient) {
        var chatMessage = {
	        "message_type" : "MESG",
	        "type" : "CHAT",
	        "user" : {
	        		"user_id": username
	        },
	        "message" : messageInput.value,
	        "channel_url" : "sendbird_open_channel_31836_03f9acb1ca94937eff8ed904ba2d2748cbcd9fd9"
        };

        stompClient.send("/app/chat.sendMessage", {}, JSON.stringify(chatMessage));
        messageInput.value = '';
    }
    event.preventDefault();
}


function onMessageReceived(payload) {
    var message = JSON.parse(payload.body);
    console.log(message);

    var messageElement = document.createElement('li');

    if(message.type === 'JOIN') {
    } else if (message.type === 'LEAVE') {
        messageElement.classList.add('event-message');
        message.content = message.sender + ' left!';
    } else {
        messageElement.classList.add('chat-message');

        var avatarElement = document.createElement('i');
        var avatarText = document.createTextNode(message.user.user_id);
        avatarElement.appendChild(avatarText);
        avatarElement.style['background-color'] = getAvatarColor(message.user.user_id);

        messageElement.appendChild(avatarElement);

        var usernameElement = document.createElement('span');
        var usernameText = document.createTextNode(message.user.user_id);
        usernameElement.appendChild(usernameText);
        messageElement.appendChild(usernameElement);
    }

    var textElement = document.createElement('p');
    var messageText = document.createTextNode(message.message);
    textElement.appendChild(messageText);

    messageElement.appendChild(textElement);

    messageArea.appendChild(messageElement);
    messageArea.scrollTop = messageArea.scrollHeight;
}

function loadMessages() {
    $.ajax({
        type: "get",
        url: "http://localhost:8080/getMessages?channel_url=" + "sendbird_open_channel_31836_03f9acb1ca94937eff8ed904ba2d2748cbcd9fd9",
        dataType: "json",
        crossDomain: true,
        complete: function (data) {
        		var messages = data.responseJSON.messages;
	        	for(var i = 0; i < data.responseJSON.messages.length; i++) {
	        		var messageElement = document.createElement('li');
                messageElement.classList.add('chat-message');

                var avatarElement = document.createElement('i');
                var avatarText = document.createTextNode(messages[i].user.user_id);
                avatarElement.appendChild(avatarText);
                avatarElement.style['background-color'] = getAvatarColor(messages[i].user.user_id);

                messageElement.appendChild(avatarElement);

                var usernameElement = document.createElement('span');
                var usernameText = document.createTextNode(messages[i].user.user_id);
                usernameElement.appendChild(usernameText);
                messageElement.appendChild(usernameElement);
                
                var textElement = document.createElement('p');
                var messageText = document.createTextNode(messages[i].message);
                textElement.appendChild(messageText);

                messageElement.appendChild(textElement);

                messageArea.appendChild(messageElement);
                messageArea.scrollTop = messageArea.scrollHeight;
	        	}
        }
    });
}


function getAvatarColor(messageSender) {
    var hash = 0;
    for (var i = 0; i < messageSender.length; i++) {
        hash = 31 * hash + messageSender.charCodeAt(i);
    }

    var index = Math.abs(hash % colors.length);
    return colors[index];
}
$(document).ready(function () {
	 connect();
	 loadMessages();
});
messageForm.addEventListener('submit', sendMessage, true)
