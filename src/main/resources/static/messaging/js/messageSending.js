function addMessageUI(messageData) {
	$('#displayMessages').append("<p><small class= 'text-primary'>" + messageData.user.nickname + ": </small>" + messageData.message + "</p>");
}

function getMessages() {
    $.ajax({
        type: "get",
        url: "http://localhost:8080/getMessages?channel_url=" + "sendbird_open_channel_31836_03f9acb1ca94937eff8ed904ba2d2748cbcd9fd9",
        dataType: "json",
        crossDomain: true,
        complete: function (data) {
        		var messages = data.responseJSON.messages;
	        	for(var i = 0; i < data.responseJSON.messages.length; i++) {
	        		addMessageUI(messages[i]);
	        	}
        }
    });
}

$(document).keypress(function(e) {
	if(e.which == 13) {
	    var inputValue = document.getElementById("sendBox").value;
	    document.getElementById("sendBox").value = '';
	    var body = JSON.stringify({ 
	            "message_type" : "MESG",
	            "user_id" : "collin_bolles",
	            "message" : inputValue,
	            "channel_url" : "sendbird_open_channel_31836_03f9acb1ca94937eff8ed904ba2d2748cbcd9fd9"
	        });
	
	    $.ajax({
	        type: "post",
	        url: encodeURI("http://localhost:8080/sendMessage"),
	        dataType: "json",
	        crossDomain: true,
	        data: body,
	        complete: function(data) {
	        		addMessageUI(data.responseJSON);
	        }
	    });
	}
});

$(document).ready(function () {
	 getMessages();
});